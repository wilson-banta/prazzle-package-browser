var path = require('path');
var fs = require('fs');
var UglifyJS = require("uglify-js");
var extend = require('util')._extend;


exports.package = function(options, cb){
	require('./package.js')(prepareOptions(options), cb);
}


exports.publish = function(options, cb){
	require('./publish.js')(prepareOptions(options), cb);
}

function packageFilepath(options){
	return path.join(options.outputDir, packageFilename(options, options) );
}

function packageFilename(options){
	return [options.project, '-', options.version, '.js'].join('');
}


function readPackageJson(){
	var projectJsonPath = path.resolve('project.json');
	if(!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;
	return require(projectJsonPath);
}


function gitVersionDnxConsole(options, cb){
	options.fn.gitVersion(options.packageJson.version, function(error, v){

		if(error) return  cb(error);

		options.version = v;

		options.log.info('Git Version Calculated: %s', options.version);

		cb(null, options);
	});
}


function prepareOptions(options){
	var publishOptions  = extend({}, options);

	publishOptions.packageJson = readPackageJson();
	publishOptions.outputDir = options.outputDir || ['..','dist', options.project].join(path.sep);

	publishOptions.browserFn = {
		packageFilepath:  packageFilepath,
		packageFilename: packageFilename,
		gitVersionDirectory: gitVersionDnxConsole
	};

	return publishOptions;
}

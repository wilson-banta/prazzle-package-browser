var async = require('async');
var fs = require('fs');
var extend = require('util')._extend;

module.exports = function(options, cb)
{
	options.log.info('Publishing Browser App To: %s', options.s3Bucket);

	async.waterfall([
		async.apply(options.browserFn.gitVersionDirectory, options),
		packgeIfNotExisting,
		uploadToS3
	], cb);

	options.browserFn.packageFilepath(options);
}

function packgeIfNotExisting(options,cb)
{
	if(!fs.existsSync(options.browserFn.packageFilepath(options))) {
		return require('./package')(options,cb);
	}

	cb(null, options);
}


function uploadToS3(options,cb)
{
	var jsFile = options.browserFn.packageFilepath(options);
 	var mapFile = jsFile + '.map';

 	uploadFileToS3(jsFile, options, cb);
 	uploadFileToS3(mapFile, options, cb);
}

function uploadFileToS3(file, options, cb)
{
	return options.fn.s3.upload({
		keyBase: 'apps',
		file: file,
		bucket: options.s3Bucket,
		name: options.project,
		version: options.version
	}, cb);
}
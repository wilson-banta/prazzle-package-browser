var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var UglifyJS = require("uglify-js");

module.exports = function(options, cb)
{
	console.log(options);
	options.log.info('Packing browser App To: %s', options.outputDir);

	async.waterfall([
		async.apply(options.browserFn.gitVersionDirectory, options),
		cleanDir,
		runNpmBuild,
		updateJsFilesToPublishDir
	], cb);
}

function cleanDir(options,cb){
	options.fn.cleanDir(options.outputDir, function(err){ cb(err,options); })
	//options.fn.cleanDir('bin', function(err) { cb(err, options); });
}

function runNpmBuild(options, cb){

	var npm_build = spawn('npm.cmd', ['run', 'build']);
	var hasError;

	npm_build.stdout.on('data', (data) => {
	  options.log.debug(data.toString());
	});

	npm_build.stderr.on('data', (data) => {
		hasError = true;
	  options.log.error(data.toString());
	});

	npm_build.on('close', (code) => {

		if(hasError) return callback('An error occured during the npm run build command.');

		options.log.info('browser package completed');

		return cb(null, options);
	});
}

function updateJsFilesToPublishDir(options,cb)
{
	var packageJsFileName = path.join('./bin', [options.project.replace('.','-').toLowerCase() , '.js'].join(''));
	var publishJsFileName = options.browserFn.packageFilepath(options);

	if(fs.existsSync(packageJsFileName))
	{
		var mapExists = fs.existsSync(packageJsFileName + '.map');

		var result = UglifyJS.minify(packageJsFileName, {
    		inSourceMap: mapExists ? packageJsFileName + '.map' : null ,
    		outSourceMap: publishJsFileName + '.map'
		});

		fs.writeFileSync(publishJsFileName, result.code);
		fs.writeFileSync(publishJsFileName + ".map", result.map);		
	}

	cb(null, options);
}

function getRequiredEnvironmentKeys(options, cb)
{
	options.log.verbose('Reading environment keys...');

	options.environmentKeys = options.fn.envHelper.read('.env.required');

	cb(null, options);
}

function writeEnvironmentKeys(options, cb){

	options.log.verbose('Writing environment keys...');

	var outputFilename = options.browserFn.packageEnvFilepath(options);

	options.fn.envHelper.writeKeys(outputFilename, options.environmentKeys);

	cb(null, options);
}
